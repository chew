module Actions::Products
  before Index do |req|
    req.session[:user].admin?
  end
  
  class Index < Action
    def setup
      @products = Product.paginate(params[:page] || 1)
    end
    
    def respond_json
      @products.to_json
    end
    
    # By default Action implements this behavior
    # so no need to state it explicitly
    #
    # def respond_html
    #   render
    # end
  end

  class Update < PostAction
    url_params ':id'
    def setup
      @product = Product.find(req.params[:id])
    end
  end
end
