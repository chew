$: << File.expand_path(File.dirname(__FILE__))

require 'chew/auxiliaries'

module Chew
  [:Action, :GetAction, :PostAction, :PutAction, :DeleteAction].each do |s|
    autoload s,  'chew/action.rb'
  end
  autoload :Const, 'chew/const.rb'
  autoload :App, 'chew/app.rb'
  autoload :Request, 'chew/request.rb'
  autoload :Router, 'chew/router.rb'
  autoload :Server, 'chew/server.rb'
  
  LIB_DIR = File.expand_path(File.dirname(__FILE__))
  SKELETON_DIR = File.expand_path(File.join(LIB_DIR, '..', 'skeleton'))
end