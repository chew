# Core extension or other utility functions
# be very wary of adding to this file.

class Module
  def tree
    submodules.inject({}) do |hash, sub|
      hash.update sub => sub.tree
    end
  end
  
  def submodules
    constants.map do |const|
      c = const_get(const)
      c if c.kind_of?(Module)
    end.compact
  end
  
  def nest(m)
    const_set(m.last_name, m)
  end
  
  def last_name
    name.split('::').last
  end
end
