# Rack handler
class Chew::App
  def initialize(router)
    @router = router
  end
  
  def call(env)
    # Dispatch the request
    request = Request.new(env, env['rack.input'])
    action_class, rel_path = @router.resolve(request)
    action = action_class.dispatch(request, rel_path)
    
    [action.status, action.headers, action.body]
  rescue => exception
    response.send_status(500)
    response.send_header
    response.write(Merb.exception(e))
    Logger.error { Merb.exception(e) }
  ensure
    Logger.flush
  end
end
