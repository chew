require 'http11' # for Mongrel::URIClassifier

module Chew
  # Users put Actions inside this module to be given automatic routing paths
  module Root
  end
  
  class Router
    def initialize
      # the keys of @classifiers are the HTTP methods
      @classifiers = Hash.new
      Chew::Request::METHODS.each do |m|
        @classifiers[m.to_sym] = Mongrel::URIClassifier.new
      end
      load_all_module_routes
    end
    
    def load_all_module_routes
      load_module_routes(Chew::Root, '/')
    end
    
    def resolve(request)
      _, rel_path, action = @classifiers[request.method].resolve(request.path_info)
      [action, rel_path]
    end
    
    private
    
    def load_module_routes(m, path)
      m.submodules.each do |n|
        n_path = File.join(path, n.last_name)
        if n < Chew::Action
          @classifiers[n.method].register(n_path, n)
        else
          load_module_routes(n, n_path)
        end
      end
    end
  end  
end