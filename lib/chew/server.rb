require 'ostruct'
require 'rack'

class Chew::Server
  DEFAULT_OPTIONS = {
    :port => '4000',
    :host => '0.0.0.0'
  }
  
  def initialize(options = {})
    options = OpenStruct.new(DEFAULT_OPTIONS.merge(options))
    
    router = Router.new
    
    handler = Handler.new(router)
    
    mongrel = Mongrel::HttpServer.new(options.host, options.port)
    mongrel.register("/", handler)
    
    Logger.info { "Running at http://#{options.host}:#{options.port}" }
    
    trap("INT") do
      Logger.info "Shutting down"
      mongrel.graceful_shutdown
      exit
    end
    mongrel.run.join
  end
end