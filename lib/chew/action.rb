class Chew::Action
  @@acceptable_method = :get
  class << self
    
    # returns the HTTP this method accepts
    def method
      :get
    end
    
    def accepts_method?(m)
      return true if method == :get and m == :head
      method == m
    end
    
    def dispatch(request, rel_path)
      unless accepts_method?(request.method)
        raise(ControllerExceptions::NotAcceptable, 
          "Action #{name} does not respond to HTTP method #{request.method}")
      end
      
      action = new(request)
      action.setup
      
      # negotiate which content type to use
      extension = request.acceptable_extensions.find do |ext| 
        responds_to.include?(ext)
      end
      
      unless extension
        raise(ControllerExceptions::NotAcceptable, 
          "Action #{name} does not respond the requested content types")
      end
      
      # set the content type
      action.headers['Content-Type'] = Const::TYPES[extension].first
      
      # render the action
      unless request.method == :head
        action.body = action.send(respond_method(extension))
      end
      
      action
    end
    
    # returns a list of extensions which the Action can respond in. for example
    # if the controller responds in text/plain and text/html, the function 
    # would return [:html, :txt]
    # the list is based on instance methods defined as "to_extension"
    def responds_to
      @@responds_to ||= begin
        Const::TYPES.keys.find_all { |ext| method_defined?(respond_method(ext)) }
      end
    end
    
    def respond_method(ext)
      "respond_" + ext.to_s
    end
  end
  
  def initialize(request)
    @_request = request
  end
  
  def setup
  end
  
  def request
    @_request
  end
  
  def session
    @_request.session
  end
  
  def cookies
    @_request.cookies
  end
  
  def params
    @_request.params
  end
  
  def headers
    @_headers ||= {}
  end
  
  def status
    @_status ||= 200
  end
  
  def status=(s)
    @_status = s
  end
  
  def body=(b)
    @_body = (b.kind_of?(String) ? StringIO.new(b) : b)
  end
  
  def body
    @_body ||= StringIO.new
  end
end

Chew::GetAction = Chew::Action

class Chew::PostAction < Chew::Action
  def self.method
    :post
  end
end

class Chew::PutAction < Chew::Action
  def self.method
    :put
  end
end

class Chew::DeleteAction < Chew::Action
  def self.method
    :delete
  end
end
