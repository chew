require File.dirname(__FILE__) + '/helper'

class Hello < Chew::Action; end

describe Chew::Action do
  before(:each) do
    @request = mock('Request')
  end
  
  it "should dispatch a simple request" do
    @request.should_receive(:method).and_return(:get)
    action = Hello.dispatch(@request, '')
    action.should be_an_instance_of(Hello)
  end
end