require File.dirname(__FILE__) + '/helper'

describe Module do
  before(:all) do
    module A
      module B
      end
      module C
        module D
        end
      end
    end
  end
  
  it "should nest" do 
    Blah = Module.new
    module Else
      nest Blah
    end
    Else.submodules.include?(Blah).should be_true
    Else::Blah.should == Blah
  end
  
  it "should list submodules" do
    A.submodules.length.should == 2
    A.submodules.include?(A::B).should be_true
    A.submodules.include?(A::C).should be_true
    
    A::B.submodules.length.should == 0    
  end
  
  it "should create a tree of submodules" do
    A.tree.should == { A::B => {}, A::C => { A::C::D => {} } }
    A::C::D.tree.should == {}
  end
end