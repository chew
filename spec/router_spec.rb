require File.dirname(__FILE__) + '/helper'

module Chew::Root
  class Hello < Chew::Action; end
  module Planets
    class Earth < Chew::Action; end
    class Mars < Chew::PostAction; end
    class Pluto < Chew::DeleteAction; end
  end
end

describe Chew::Router do
  
  before(:each) do
    @router = Chew::Router.new
    @request = mock('Request')    
  end
  
  [
    [Chew::Root::Hello, '/Hello', :get],
    [Chew::Root::Planets::Earth, '/Planets/Earth', :get],
    [Chew::Root::Planets::Mars, '/Planets/Mars', :post],
    [Chew::Root::Planets::Pluto, '/Planets/Pluto', :delete]
  ].each do |klass, path, method|
    it "should resolve #{method.to_s.upcase} request to #{path}" do
      @request.should_receive(:path_info).and_return(path)
      @request.should_receive(:method).and_return(method)
      action, rel_path = @router.resolve(@request)
      action.should == klass
      rel_path.should == ''
    end
    
    p = File.join(path, "blahblah", "blah")
    it "should resolve request to #{p} with rel_path" do
      @request.should_receive(:path_info).and_return(p)
      @request.should_receive(:method).and_return(method)
      action, rel_path = @router.resolve(@request)
      action.should == klass
      rel_path.should == File.join('/',"blahblah", "blah")
    end
    
    ([:get, :post, :put, :delete]-[method]).each do |m|
      it "should not resolve #{m.to_s.upcase} request to #{path}" do       
        @request.should_receive(:path_info).and_return(path)
        @request.should_receive(:method).and_return(m)
        action, rel_path = @router.resolve(@request)
        action.should be_nil
        rel_path.should be_nil
      end
    end
  end
  
end